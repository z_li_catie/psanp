cmake_minimum_required (VERSION 2.8)
project (PSANP)

if(UNIX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++14 -pthread -lstdc++ -lm")
endif()

include_directories(include include)
include_directories(include ${LASZIP_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})
add_executable(PSANP src/GenerateOctree.cpp src/LASPointReader.cpp)

target_link_libraries(PSANP ${LASZIP_LIBRARY} ${Boost_SYSTEM_LIBRARY} ${Boost_THREAD_LIBRARY} ${Boost_REGEX_LIBRARY} ${Boost_FILESYSTEM_LIBRARY} ${Boost_PROGRAM_OPTIONS_LIBRARY})

install(TARGETS PSANP RUNTIME DESTINATION bin/ )