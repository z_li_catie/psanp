
#include <chrono>
#include <sstream>
#include <string>
#include <map>
#include <vector>
#include <math.h>
#include <fstream>
#include "../include/Point.h"
#include "../include/OctreeConverter.h"
#include "../include/LASPointReader.h"
#include <stdio.h>
#include <cstdlib>
#include <iostream>

using std::stringstream;
using std::map;
using std::string;
using std::vector;
using std::find;
using std::fstream;
using std::cout;
using namespace PSANP;

PointReader* createPointReader(string path){
	PointReader* reader = new LASPointReader(path);
	return reader;
}

AABB calculateAABB(){
	AABB aabb;
	string source = "/home/aurore/Desktop/COMPIEGNE_EXT_EEM_Bat19_EXT_1134.laz";
	PointReader* reader = createPointReader(source);
	
	AABB lAABB = reader->getAABB();
	aabb.update(lAABB.min);
	aabb.update(lAABB.max);

	reader->close();
	delete reader;
	
	return aabb;
}

void convert(){

	//prepare();
	string source = "/home/aurore/Desktop/COMPIEGNE_EXT_EEM_Bat19_EXT_1134.laz";
	long long pointsProcessed = 0;

	AABB aabb = calculateAABB();
	cout << "AABB: " << endl << aabb << endl;
	aabb.makeCubic();
	cout << "cubic AABB: " << endl << aabb << endl;


	cout << "READING:  " << source << endl;

	PointReader* reader = createPointReader("/home/aurore/Desktop/COMPIEGNE_EXT_EEM_Bat19_EXT_1134.laz");
	while(reader->readNextPoint()){
		pointsProcessed++;

		Point p = reader->getPoint();

		stringstream ssMessage;

		ssMessage.imbue(std::locale(""));
		ssMessage << "INDEXING: ";
		ssMessage << p.position << endl;

		cout << ssMessage.str() << endl;
	}


	reader->close();
	delete reader;
	
	cout << endl;
	cout << "conversion finished" << endl;
}

int main()
{
	//path pSource("/home/aurore/Desktop/COMPIEGNE_EXT_EEM_Bat19_EXT_1134.laz");
	//if(boost::filesystem::is_regular_file(pSource)){
		//cout << "regular file" << endl;
	//}

	convert();
}

