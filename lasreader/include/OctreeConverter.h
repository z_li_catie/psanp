#ifndef POTREE_CONVERTER_H
#define POTREE_CONVERTER_H

#include "AABB.h"
#include "PointReader.h"

#include <string>
#include <vector>
#include <cstdint>

using std::vector;
using std::string;

namespace PSANP {

class OctreeConverter{

private:
	AABB aabb;
	vector<string> sources;
	string workDir;

	PointReader* createPointReader(string source);
	void prepare();
	AABB calculateAABB();

public:
	int maxDepth;
	string format;
	vector<double> colorRange;
	vector<double> intensityRange;
	vector<double> aabbValues;

	OctreeConverter(string workDir, vector<string> sources);
		
	void convert();

};

}

#endif